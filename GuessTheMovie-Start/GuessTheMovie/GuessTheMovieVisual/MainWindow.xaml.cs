﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GuessTheMovieVisual
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MoviesController _movieController;

        public MainWindow()
        {
            _movieController = new MoviesController();
            InitializeComponent();

            searchButton.Click += GetMovie;
        }

        private void GetMovie(object sender, RoutedEventArgs e)
        {
           
            var movie = _movieController.GetMovieByName(movieSearchBox.Text).Result;

            movieTitleLbl.Content = movie.Title;
            ratedLbl.Content = movie.Rated;
            releasedLbl.Content = movie.Released;
            directorLbl.Content = movie.Director;
            writerLbl.Content = movie.Writer;
            actorsLbl.Content = movie.Actors;
            plotLbl.Content = movie.Plot;
        }

        private void movieSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
