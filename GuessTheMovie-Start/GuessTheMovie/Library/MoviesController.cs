﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Diagnostics;
using System.Net;
using Library.Models;

namespace Library
{
    public class MoviesController
    {
        HttpClient Client { get; set; }

        public MoviesController()
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("http://www.omdbapi.com/");
        }

        public async Task<Movie> GetMovieByName(string movieName)
        {
            try
            {
                HttpResponseMessage response = Client.GetAsync("?t=" + movieName).Result;

                return await response.Content.ReadAsAsync<Movie>();

            }
            catch (Exception e)
            {
                var x = e;
                throw new Exception(e.Message + " on " + nameof(GetMovieByName));
            }
        }
    }
}
